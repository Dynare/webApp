<a name="logo"/>
<div align="center">
<a href="https://git.dynare.org/Dynare/webApp" target="_blank">
<img src="https://git.dynare.org/uploads/-/system/project/avatar/126/dynareApp.jpg?width=64" width="64" height="64" alt="Dynare webApp Logo"></img>
</a>
</div>

# Dynare webApp

webApp is a Graphical User Interface (GUI) for Dynare that embeds a Dynare code editor, an output console and graphical reporting of results. It offers an unified Dynare user experience with graphically enhanced functionalities. webApp jointly uses several technologies. The interface is rendered using web technologies whereas the computations are performed using [Dynare](http://www.dynare.org/) and a numerical computing environment such as [MatLab](https://www.mathworks.com/products/matlab.html?s_tid=hp_products_matlab). In more details, the server side elements of the webApp are written in NodeJS and the front side are written in Javascript.

 
# License
Most of the source files are covered by the GNU General Public Licence version
3 or later. Please read the attached LICENSE file.

# Installing the webApp

[Node.js](https://nodejs.org/en/) is required to run the webApp and [npm](https://www.npmjs.com/) is the default package manager for the project. A fresh install of [Dynare](https://www.dynare.org/) and a Dynare compatible version of [MATLAB](https://www.mathworks.com/products/matlab.html) are also required.

To run the development version of the webApp, follow these steps:
- Download and install NodeJS and npm,
- Download the webApp source, 
- cd to the webApp directory and run `npm install` to install required dependencies,
- From the Dynare directory, copy the preprocessor binary from `/matlab/preprocessor64` to the webApp `/assets/preprocessor` directory
- Edit the `/assets/dynareapp.JSON` file and enter the path to your Matlab binary path and the Dynare install directory path
- In the webApp directory run `npm start` to run the app
- The default port is 3000, the webApp should be available at `http://localhost:3000/`

# Documentation

A small user guide for the perfect-foresight and stochastic simulation modules can be found under `/doc` or by following [webApp_doc_latest.pdf](https://git.dynare.org/Dynare/webApp/-/blob/master/doc/webApp_doc_latest.pdf)