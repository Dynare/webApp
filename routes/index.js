var express = require('express');
var router = express.Router();
var bodyParser = require('body-parser');
const formidableMiddleware = require('express-formidable');
var csrf = require('csurf');
var cookieParser = require('cookie-parser');

const getmodelfromdb = require('../appModules').getmodelfromdb;
const savetodb = require('../appModules').savetodb;
const newmodelindb = require('../appModules').newmodelindb;
const movenewdatafile = require('../appModules').movenewdatafile;
const setpreprocess = require('../middleware/runprocesses').setpreprocess;
const setpreprocessmore = require('../middleware/runprocesses').setpreprocessmore;
const runstochasticnode = require('../middleware/runprocesses').runstochasticnode;
const runperfectnode = require('../middleware/runprocesses').runperfectnode;
const runestimationnode = require('../middleware/runprocesses').runestimationnode;
// const ipc = require('node-ipc');

const csrfProtection = csrf({
  cookie: true
});
router.use(bodyParser.urlencoded({
  extended: true
}));
router.use(cookieParser());
router.use(csrfProtection);






// router.use(bodyParser.json());



// var parseForm = bodyParser.urlencoded({ extended: true })

// ipc.config.id = 'routerIPC';
// ipc.config.retry = 1500;
// // ipc.config.silent = true;

// ipc.connectTo('appIPC', () => {
//   ipc.of.appIPC.on('connect', () => {
//     ipc.of.appIPC.emit('rundynarematlab', "hello");
//   });
// });






/* GET home page. */
router.get('/', function (req, res, next) {
  //get will just show the last saved model

  // console.log(req.csrfToken())

  //this is the way to handle promised elements (here a database call)
  getmodelfromdb()
    .then(function (data) {
      // console.log(data);
      res.render('index', {
        layout: false,
        csrfToken: req.csrfToken(),
        dynaremodel: data
      });
    })
    .catch(function (e) {
      res.status(500, {
        error: e
      });
    });

});


//body-parser does not handle file uploads
//express-formidable does handle both files and text but to avoid changing the whole code there is a route just for files
router.post('/fileroute', formidableMiddleware(), async (req, res) => {
  const file = req.files.file;
  const filepath = req.files.file.path;
  // console.log('file info: ' + file+' '+filepath);

  const fields = req.fields;
  // console.log('fields = ' + JSON.stringify(fields));

  // console.log(fields.filename)

  movenewdatafile(fields.filename, fields.filetype, filepath)
  .then(function (data) {
    // console.log(data);
    // res.json(data);
    res.json({
      'status': 1
    });
  })
  .catch(function (e) {
    console.log(e.toString());
    res.json({
      'status': 200,
      'message': e.toString()
  });
  });


});


router.post('/', function (req, res) {

  console.log(req.body.accesstype);
  // console.log('In POST');

  switch (req.body.accesstype) {
    case 'save':
      // this is a simple database save/autosave
      savetodb(req.body)
        .then(function (data) {
          // console.log(data);
          res.json({
            'status': 1
          });
        })
        .catch(function (e) {
          res.json({
            'status': 999,
            'message': 'Unable to save model to database.' + e
          });
        });
      break;

    case 'newproject':
      // the user requested the creation of a new model
      newmodelindb(req.body.modelname)
        .then(function (data) {
          // console.log(data);
          res.json({
            'status': 1
          });
        })
        .catch(function (e) {
          // res.json({
          //   'status': 999,
          //   'message': 'Unable to create new model in database.'+e
          // });
          console.log(e);
          res.status(500, {
            error: e
          });
        });
      break;

    case 'savepreprocess':
      //this is a db save + preprocessor run

      setpreprocess(req.body)
        .then(function (data) {
          console.log(data);
          res.json(data);
        })
        .catch(function (e) {
          res.json(e);
        });
      break;


    case 'setstochastic':
      setpreprocessmore(req.body)
        .then(function (data) {
          console.log(data);
          res.json(data);
        })
        .catch(function (e) {
          res.json(e);
        });
      break;


    case 'setperfect':
      console.log('Got setperfect');
      setpreprocessmore(req.body)
        .then(function (data) {
          console.log(data);
          res.json(data);
        })
        .catch(function (e) {
          res.json(e);
        });
      break;


    case 'setestimation':
      setpreprocessmore(req.body)
        .then(function (data) {
          console.log(data);
          res.json(data);
        })
        .catch(function (e) {
          res.json(e);
        });
      break;

    default:
      res.json({
        'status': 999,
        'message': 'Unable to process request.'
      });

  }

});

router.put('/', function (req, res) {

  console.log('put received');
  console.log(req.body);

  switch (req.body.accesstype) {

    case 'runestimation':

    console.log('runestimation got it');


      runestimationnode(req.body)
      .then(function (data) {
        console.log(data);
        res.json(data);
      })
      .catch(function (e) {
        res.json(data);
      });

    break;

    case 'runstochastic':

      runstochasticnode(req.body)
      .then(function (data) {
        console.log(data);
        res.json(data);
      })
      .catch(function (e) {
        res.json(data);
      });

    break;


    case 'runperfect':

      console.log('Got runperfect');

      runperfectnode(req.body)
      .then(function (data) {
        console.log(data);
        res.json(data);
      })
      .catch(function (e) {
        res.json(data);
      });

    break;


    default:
      res.json({
        'status': 999,
        'message': 'Unable to process request.'
      });

  }


});


module.exports = router;
