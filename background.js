const path = require('path');
const spawn = require('child_process').spawn;
const loadjsonfileatpath = require('./appModules').loadjsonfileatpath;
var origincalltype = 0;



// let the main thread know this thread is ready to process something
// function ready() {
//   console.log('ready to do stuff')
//   mymessage={'messageid': 'ready','message': 'ready' };
// 	process.send(mymessage);
// }

async function main() {
  //this function loads matlab and then relays all commands to matlab

  // loading the paths
  try {
    var pathholder = await loadjsonfileatpath('dynareapp.JSON', './assets/');
  } catch (error) {
    console.log("Can't load dynareapp.JSON" + error);
    return false;
  }


  //loading matlab
  var exePath = path.resolve(__dirname, pathholder.matlabpath + '/bin/matlab');
  const child = spawn(exePath, ['-nosplash -nodisplay'], {
    shell: true
  });

  child.on('exit', function (code, signal) {
    console.log('child process exited with ' +
      `code ${code} and signal ${signal}`);
  });

  child.stdout.on('data', (data) => {
    // console.log(`child stdout:\n${data}`);

    // process.send("Receiving messages");

    //this captures the process stdout ">>" as a sign of readyness in ascii code
    if ((data[0] == 62) && (data[1] == 62) && (data[2] = 32)) {
      if (origincalltype == 0) {
        mymessage = {
          'messageid': 'matlabready',
          'message': 0
        };
      }
      if (origincalltype == 1) {
        //this is the return from a stochastic simulation
        mymessage = {
          'messageid': 'matlabready',
          'message': 1
        };
      }
      if (origincalltype == 2) {
        //this is the return from a perfect foresight computation
        mymessage = {
          'messageid': 'matlabready',
          'message': 2
        };
      }

      if (origincalltype == 3) {
        //this is the return from estimation computation
        mymessage = {
          'messageid': 'matlabready',
          'message': 3
        };
      }

      // process.send('matlabready', origincalltype);
      process.send(mymessage);
      // }
      // if (origincalltype == 0) {
      //   origincalltype += 1;
      // }
    } else {

      data = data.toString();

      // if (data[0] == 8){
      if (data.includes('RW Metropolis-Hastings') == true) {
        //capturing RW Metropolis-Hastings (1/2) Current acceptance ratio wrong char
        // data=data.slice(69).toString();
        // console.log(data);
        // data=data+'\n';
        mymessage = {
          'messageid': 'stdoutmessage',
          'messagetype': 'progressbar',
          'message': data //'message': `${data}`
        };
      } else if (data.includes('Bayesian (posterior) IRFs...') == true) {
        //capturing Bayesian (posterior) IRFs...
        mymessage = {
          'messageid': 'stdoutmessage',
          'messagetype': 'progressbarBAYES',
          'message': data //'message': `${data}`
        };
      } else if (data.includes('Taking posterior subdraws...') == true) {
        //capturing smoother posterior subdraws...
        mymessage = {
          'messageid': 'stdoutmessage',
          'messagetype': 'progressbarSMOOTH',
          'message': data //'message': `${data}`
        };
      } else {
        // data = data.toString();
        mymessage = {
          'messageid': 'stdoutmessage',
          'messagetype': 'normal',
          'message': data //'message': `${data}`
        };
      }


      process.send(mymessage);
    }
  });

  child.stderr.on('data', (data) => {
    console.error(`child stderr:\n${data}`);
  });

  var filePathcd = path.resolve(__dirname, './assets/modfiles/');
  child.stdin.setEncoding('utf-8');
  child.stdin.write("addpath('" + pathholder.dynarepath + "/matlab');dynare_config;cd " + filePathcd + ";\n");

  //process messages from parent
  process.on('message', (pmessage) => {
    // var messageprocess=0;



    if (pmessage.messageid == 'rundynarematlab') {


      if (pmessage.messagetype == 'stochastic') {
        origincalltype = 1;
        child.stdin.write('run temp.driver.m;run di_stochastic_simulations.m;\n');
        child.stdin.on('drain', () => {});
      }

      if (pmessage.messagetype == 'perfect') {
        origincalltype = 2;
        child.stdin.write('run temp.driver.m;run di_perfect_foresight.m;\n');
        child.stdin.on('drain', () => {});
      }

      if (pmessage.messagetype == 'estimation') {
        origincalltype = 3;
        child.stdin.write('run temp.driver.m;run di_estimation.m;\n');
        child.stdin.on('drain', () => {});
      }



    }

    // mymessage={'messageid': 'stdoutmessage','message': 'received rundynarematlab in background' };
    // process.send(mymessage);



  });






}



main();