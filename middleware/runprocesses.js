const savetodb = require('../appModules').savetodb;
const removeoldfile = require('../appModules').removeoldfile;
const createnewfile = require('../appModules').createnewfile;
const preprocess = require('../appModules').preprocess;
const loadjsonfile = require('../appModules').loadjsonfile;
const checkfileexist = require('../appModules').checkfileexist;
const preparejsonfiles = require('../appModules').preparejsonfiles;
const path = require('path');
var fs = require('fs');

const ipc = require('node-ipc');

module.exports = {

    // async function setpreprocess(modelcode) {
    setpreprocess: (modelcode) => {
        // will save and preprocess the model

        return new Promise(async function (resolve, reject) {

            var dynaremodel = '';
            // saving to database and retrieving the model from database
            try {
                dynaremodel = await savetodb(modelcode);
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // removing old files
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini_steady_state_model.json');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini.json');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini.mod');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // creating a modfile from editor input. Flag 'w' truncates if file already exits
            try {
                await createnewfile(dynaremodel['modelhash'] + '_ini.mod', dynaremodel['modelcode']);
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // calling dynare preprocessor on modfile
            var dynarestdout = '';
            var preprocok = 1;
            try {
                // dynarestdout += await preprocess(dynaremodel['modelhash'] + '_ini.mod', 3);//does not work for some reason...
                dynarestdout += await preprocess(dynaremodel['modelhash'] + '_ini.mod', 1);
            } catch (error) {
                dynarestdout += error.toString();
                preprocok = 0;
            }

            if (preprocok == 0) {
                return reject({
                    'status': 100,
                    'message': dynarestdout
                });
            } else {
                return resolve({
                    'status': 1,
                    'message': dynarestdout
                });
            }

        }); //end of main promise
    },

    setpreprocessmore: (modelcode) => {
        // will save and run a stochastic simulation step

        return new Promise(async function (resolve, reject) {

            var dynaremodel = '';
            // saving to database and retrieving the model from database
            try {
                dynaremodel = await savetodb(modelcode);
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // checking if SS info is available
            var extrassinfo = (dynaremodel['modelextrassinfo'] != null && dynaremodel['modelextrassinfo'].length < 1) ? (dynaremodel['modelextrassinfo']) : null
            var extrasstype = (dynaremodel['modelextrasstype'] != null && dynaremodel['modelextrasstype'] > 0) ? (dynaremodel['modelextrasstype']) : null

            // removing old files
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini_steady_state_model.json');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini.json');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }
            try {
                await removeoldfile(dynaremodel['modelhash'] + '_ini.mod');
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // creating a modfile from editor input. Flag 'w' truncates if file already exits
            try {
                await createnewfile(dynaremodel['modelhash'] + '_ini.mod', dynaremodel['modelcode']);
            } catch (error) {
                return reject({
                    'status': 200,
                    'message': error.toString()
                });
            }

            // calling dynare preprocessor on modfile
            var dynarestdout = '';
            var preprocok = 1;
            try {
                dynarestdout += await preprocess(dynaremodel['modelhash'] + '_ini.mod', 1);
            } catch (error) {
                dynarestdout += error.toString();
                preprocok = 0;
            }
            console.log(dynarestdout)


            if (preprocok == 0) {
                return reject({
                    'status': 100,
                    'message': dynarestdout
                });
            }

            //starting with Dynare 4.6 the JSON file is created in a subfolder and it is called 'modfile.json'
            //for simplicity we move it out of there and delete the folder

            new Promise(function (resolve, reject) {
                var oldfilepath = path.resolve(__dirname, '../assets/modfiles/' + dynaremodel['modelhash'] + '_ini/model/json/modfile.json');
                console.log(oldfilepath)
                var newfilePath = path.resolve(__dirname, '../assets/modfiles/' + dynaremodel['modelhash'] + '_ini.json');
                console.log(newfilePath)
                fs.rename(oldfilepath, newfilePath, function (err) {
                    if (err) return reject(new Error('Unable to move model JSON to work folder: ' + err));
                    return resolve();
                });
            });



            new Promise(function (resolve, reject) {
                var oldfilepath = path.resolve(__dirname, '../assets/modfiles/' + dynaremodel['modelhash'] + '_ini/model/json/steady_state_model.json');
                var newfilePath = path.resolve(__dirname, '../assets/modfiles/' + dynaremodel['modelhash'] + '_ini_steady_state_model.json');
                fs.rename(oldfilepath, newfilePath, function (err) {
                    if (err) return reject(new Error('Unable to move steady_state_model JSON to work folder: ' + err));
                    return resolve();
                });
            });

            //careful the following rmdir function requires node version 12 or higher 
            new Promise(function (resolve, reject) {
                var oldfilepath = path.resolve(__dirname, '../assets/modfiles/' + dynaremodel['modelhash'] + '_ini/');
                fs.rmdir(oldfilepath, {
                    recursive: true
                }, function (err) {
                    if (err) return reject(new Error('Unable to remove directory: ' + err));
                    return resolve();
                });
            });



            // loading JSON created by dynare preprocessor
            try {
                var dynaremodeljson = await loadjsonfile(dynaremodel['modelhash'] + '_ini.json');
            } catch (error) {
                // return popthis('Error', 'Unable to open file ' + basedir(dynaremodel['modelhash'] + '_ini.json'));
                return reject({
                    'status': 200,
                    'message': 'Unable to open file ' + basedir(dynaremodel['modelhash'] + '_ini.json')
                });
            }
            // console.log(dynaremodeljson);
            // showconsolealert(dynarestdout);

            if (dynaremodeljson['exogenous'] == null || dynaremodeljson['exogenous'].length < 1) {
                return reject({
                    'status': 200,
                    'message': 'You did not define any varexo for this model. Please define shocks before using any simulation.'
                });
            } else {
                //we verify if a way to compute the steady state has been provided: either steady_state_block or initval
                let checkexistflag = await checkfileexist(dynaremodel['modelhash'] + '_ini_steady_state_model.json');
                if (checkexistflag == true) {
                    return resolve({
                        'status': 1,
                        'message': dynarestdout,
                        'modeljson': dynaremodeljson
                    });
                } else {
                    // there is no steady_state_block, we check for initval block (for now we ignore steadystate file)
                    let matchflag = 0;
                    Object.keys(dynaremodeljson['statements']).forEach((key, index) => {
                        if (dynaremodeljson['statements'][key]['statementName'] == 'init_val') {
                            matchflag = 1;
                        }
                    });

                    if (matchflag == 1) {
                        // initval block found
                        return resolve({
                            'status': 2,
                            'message': dynarestdout,
                            'modeljson': dynaremodeljson
                        });
                    } else {
                        // initval block not found: we provide the last known steadystate available in the database
                        return resolve({
                            'status': 201,
                            'message': dynarestdout,
                            'modeljson': dynaremodeljson,
                            'extrassinfo': extrassinfo,
                            'extrasstype': extrasstype
                        });
                    }
                }
            }
        }); //end of main promise
    },

    runestimationnode: (estimval) => {

        return new Promise(async function (resolve, reject) {

            var modelhash = estimval['modelhash'];
            // preparing only the standardized JSON in data
            var estimindata = {
                'optionsdescription': estimval['optionsdescription'],
                'obsdescription': estimval['obsdescription'],
                'pardescription': estimval['pardescription'],
                'datafiletype': estimval['datafiletype']
            };

            try {
                await preparejsonfiles(modelhash, 3, estimval, estimindata);
            } catch (error) {
                return reject(error);
            }

            ipc.config.id = 'routerIPC';
            ipc.config.retry = 1500;
            ipc.connectTo('appIPC', () => {
                ipc.of.appIPC.on('connect', () => {
                    ipc.of.appIPC.emit('rundynarematlab', "estimation");
                    ipc.disconnect('appIPC');
                });
            });

            return resolve('Done runstochasticnode !');


        }); //end of main promise


    },

    runstochasticnode: (stosimval) => {

        return new Promise(async function (resolve, reject) {

            var modelhash = stosimval['modelhash'];
            // preparing only the standardized JSON in data
            var stochsimindata = {
                'optionsdescription': stosimval['optionsdescription'],
                'stochasticshocksdescription': stosimval['stochasticshocksdescription']
            };

            try {
                await preparejsonfiles(modelhash, 2, stosimval, stochsimindata);
            } catch (error) {
                return reject(error);
            }

            ipc.config.id = 'routerIPC';
            ipc.config.retry = 1500;
            ipc.connectTo('appIPC', () => {
                ipc.of.appIPC.on('connect', () => {
                    ipc.of.appIPC.emit('rundynarematlab', "stochastic");
                    ipc.disconnect('appIPC');
                });
            });

            return resolve('Done runstochasticnode !');


        }); //end of main promise


    },

    runperfectnode: (perfectval) => {

        return new Promise(async function (resolve, reject) {

            var modelhash = perfectval['modelhash'];
            // preparing only the standardized JSON in data
            var perfectindata = {
                'exonum': perfectval['exonum'],
                'permanentshockexist': perfectval['permanentshockexist'],
                'transitoryshockexist': perfectval['transitoryshockexist'],
                'nonanticipatedshockexist': perfectval['nonanticipatedshockexist'],
                'delayexist': perfectval['delayexist'],
                'permanentshocksdescription': perfectval['permanentshocksdescription'],
                'shocksdescription': perfectval['shocksdescription'],
                'optionsdescription':perfectval['optionsdescription'],
                'nonanticipmatrix': perfectval['nonanticipmatrix']
            };


            try {
                await preparejsonfiles(modelhash, 1, perfectval, perfectindata);
            } catch (error) {
                return reject(error);
            }

            console.log('before IPC')

            ipc.config.id = 'routerIPC';
            ipc.config.retry = 1500;
            ipc.connectTo('appIPC', () => {
                ipc.of.appIPC.on('connect', () => {
                    ipc.of.appIPC.emit('rundynarematlab', "perfect");
                    ipc.disconnect('appIPC');
                });

            });

            return resolve('Done runperfectnode !');


        }); //end of main promise






    }









}
