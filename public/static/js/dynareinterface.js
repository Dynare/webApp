var socket = io();

function setfilename(val) {
  var fileName = val.substr(val.lastIndexOf("\\") + 1, val.length);
  document.getElementById("uploadFile").value = fileName;
}

function IsNumeric(input) { //is this a number ?
  input = input.replace(/\./g, '');
  return (input - 0) == input && ('' + input).trim().length > 0;
}

function popthis(title, content) { //pops an alert
  $.alert({
    title: title,
    content: content,
  });
}

function cl(content) {
  return (console.log(content));
}

function sortbyfirst(a, b) {
  if (a[0] === b[0]) {
    return 0;
  } else {
    return (a[0] < b[0]) ? -1 : 1;
  }
}

// create a file download link
function download(filename, text) {
  var element = document.createElement('a');
  element.setAttribute('href', 'data:text/plain;charset=utf-8,' + encodeURIComponent(text));
  element.setAttribute('download', filename);
  element.style.display = 'none';
  document.body.appendChild(element);
  element.click();
  document.body.removeChild(element);
}

function scrolltextarea() {
  var textarea = document.getElementById('preprocessorout');
  textarea.scrollTop = textarea.scrollHeight;
}

function showconsolealert(content, showflag) {
  //writes to the console
  let newcontent = $("textarea#preprocessorout").val() + content;
  $("textarea#preprocessorout").val(newcontent);
  if (showflag == 1) {
    $('.nav-tabs a[href="#console"]').tab('show');
  }

}

// function createNewModel = (modeltitle,modelhash,modelname,modelcode) => {
function createNewModel(modeltitle, modelhash, modelname, modelcode) {
  document.title = modeltitle + ' Project';
  $('#codehash').val(modelhash);
  $('#codename').val(modelname);
  $('#code-dynare').val(modelcode);

  var te_dynare = document.getElementById("code-dynare");
  // te_dynare.value = $('#code-dynare').val();//"%Enter model here\n"

  window.editor_dynare = CodeMirror.fromTextArea(te_dynare, {
    mode: "octave",
    lineNumbers: true,
    viewportMargin: Infinity,
    extraKeys: {
      "Ctrl-Q": function (cm) {
        cm.foldCode(cm.getCursor());
      }
    },
    foldGutter: true,
    gutters: ["CodeMirror-linenumbers", "CodeMirror-foldgutter"]
  });

  return;
}



//autosave
var int;
var autosaveinterval = 30000;
$(document).ready(function () {
  int = window.setInterval("autosavecall();", autosaveinterval);
});

function autosavecall() {
  var modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }

  var modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': modelname,
    'accesstype': 'save'
  };
  $.ajax({
    url: $(editorform).attr('action'),
    headers: {
      'x-csrf-token': $('#_csrf').val()
    },
    type: "POST",
    data: modelcode,
    success: function (resp) {
      if (resp.status == 999) {
        popthis('Error', resp.message);
      } else {
        var currentdate = new Date();
        $('#classlist-form-messages').text('Last saved at: ' + currentdate);
      }

    },
    error: function () {
      console.log('there was a problem checking the fields');
    }
  });
  return false;
}

//button save code
$('#savecode').click(function () {

  var modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }

  var modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': modelname,
    'accesstype': 'save'
  };

  // savecodeButton(modelcode);

  $.ajax({
    url: $(editorform).attr('action'),
    headers: {
      'x-csrf-token': $('#_csrf').val()
    },
    type: "POST",
    data: modelcode,
    success: function (resp) {

      console.log(resp)

      if (resp.status == 999) {
        popthis('Error', resp.message);
      } else {
        var currentdate = new Date();
        $('#classlist-form-messages').text('Last saved at: ' + currentdate);
      }
    },
    error: function () {
      console.log('there was a problem checking the fields');
    }
  });
  return false;
});

//button clear console
$('#consoleclearbtn').click(function () {
  document.getElementById("preprocessorout").value='';
});





//new project on click function
function createnewproject() {

  editorformurl = $(editorform).attr('action');

  $.confirm({
    title: 'New project',
    content: '' +
      '<form action="" class="formName">' +
      '<div class="form-group">' +
      '<label>Please enter a name for the new project:</label>' +
      '<input type="text" placeholder="Project name" class="newprojectname form-control" required />' +
      '</div>' +
      '</form>',
    buttons: {
      formSubmit: {
        text: 'Create',
        btnClass: 'btn-blue',
        action: function () {
          var newname = this.$content.find('.newprojectname').val();
          if (!newname) {
            $.alert('Please provide a valid name');
            return false;
          }
          var modelcode = {
            'modelcode': 'nothing',
            'modelhash': 'nothing',
            'modelname': newname,
            'accesstype': 'newproject'
          };
          return $.ajax({
            url: editorformurl,
            headers: {
              'x-csrf-token': $('#_csrf').val()
            },
            type: "POST",
            data: modelcode,
            success: function (resp) {
              // var currentdate = new Date();
              // $('#classlist-form-messages').text('Last saved at: '+ currentdate);
              if (resp.status == 1) {
                //we reload the page
                var loc = window.location;
                //this will reload with get forcing python to request the latest model from db
                window.location = loc.protocol + '//' + loc.host + loc.pathname + loc.search;
              }
            },
            error: function () {
              console.log('there was a problem checking the fields');
            }
          });
        }
      },
      cancel: function () {
        //close
      },
    },
    onContentReady: function () {
      // bind to events
      var jc = this;
      this.$content.find('form').on('submit', function (e) {
        // if the user submits the form by pressing enter in the field.
        e.preventDefault();
        jc.$$formSubmit.trigger('click'); // reference the button and click it
      });
    }
  });

}




//button preprocess
$('#runcode').click(function () {

  var modelname = $('#codename').val();
  if (modelname == null || modelname == "") {
    $.alert({
      title: 'Project name is empty.',
      content: 'Please choose a name for this project...',
    });
    return false;
  }


  var modelcode = {
    'modelcode': window.editor_dynare.getValue(),
    'modelhash': $('#codehash').val(),
    'modelname': modelname,
    'accesstype': 'savepreprocess'
  };
  $.ajax({
    url: $(editorform).attr('action'),
    headers: {
      'x-csrf-token': $('#_csrf').val()
    },
    type: "POST",
    data: modelcode,
    success: function (resp) {

      if (resp.status == 200) {
        console.log("200")
        return popthis('Error', resp.message);
      } else if (resp.status == 100) {
        console.log("100")
        showconsolealert(resp.message, 1);
        scrolltextarea();
        console.log('console error');
        return false;
      }
      // else {


      $("textarea#preprocessorout").val(resp.message);
      $('.nav-tabs a[href="#console"]').tab('show');
      var currentdate = new Date();
      $('#classlist-form-messages').text('Last saved at: ' + currentdate);



    },
    error: function () {
      console.log('there was a problem checking the fields');
    }
  });
  return false;
});


// socketIO

$(function () {
  socket.on('matlabmessagetoconsole', function (msg) {
    showconsolealert(`${msg}`, 0);
    scrolltextarea();
  });
});

var numberPattern = /\d+/g;
// var modalupdatecount;

$(function () {
  //initializes the MH progress bar
  socket.on('matlabmessagetoconsole_progressbar_init', function (msg) {

    $('#MHmodal').modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#MHmodal').modal('show');
    // modalupdatecount=0;
    // console.log('matlabmessagetoconsole_progressbar_init received');
    // document.getElementById("MHloop").innerHTML="1";
  });
});

$(function () {
  //updates the MH progress bar
  socket.on('matlabmessagetoconsole_progressbar', function (msg) {
    // console.log('matlabmessagetoconsole_progressbar received');

    // modalupdatecount++;
    // if (modalupdatecount%10==0){
    //   $('#MHmodal').modal('show');
    // }

    var mynumarray = msg.match(numberPattern);
    document.getElementById("MHloop").innerHTML = mynumarray[0];
    document.getElementById("MHloopover").innerHTML = mynumarray[1];
    document.getElementById("MHCAR").innerHTML = mynumarray[2].toString() + '.' + mynumarray[3].toString();

    var elem = document.getElementById("myBar");
    var width = mynumarray[4];

    if (width >= 100) {
      i = 0;
    } else {
      // width++;
      elem.style.width = width + "%";
      elem.innerHTML = width + "%";
    }

  });
});


$(function () {
  //initializes the Bayesian IRF progress bar
  socket.on('matlabmessagetoconsole_progressbarbayes_init', function (msg) {
    $('#MHmodal').modal('hide');
    $('#BImodal').modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#BImodal').modal('show');
  });
});

$(function () {
  //updates the Bayesian IRF progress bar
  socket.on('matlabmessagetoconsole_progressbarbayes', function (msg) {
    var mynumarray = msg.match(numberPattern);
    var elem = document.getElementById("myBIBar");
    var width = mynumarray[0];

    if (width >= 100) {
      i = 0;
    } else {
      // width++;
      elem.style.width = width + "%";
      elem.innerHTML = width + "%";
    }
  });
});



$(function () {
  //initializes the Bayesian IRF progress bar
  socket.on('matlabmessagetoconsole_progressbarsmooth_init', function (msg) {
    $('#MHmodal').modal('hide');
    $('#BImodal').modal('hide');
    $('#SMmodal').modal({
      backdrop: 'static',
      keyboard: false
    });
    $('#SMmodal').modal('show');
  });
});

$(function () {
  //updates the Bayesian IRF progress bar
  socket.on('matlabmessagetoconsole_progressbarsmooth', function (msg) {
    var mynumarray = msg.match(numberPattern);
    var elem = document.getElementById("mySMBar");
    var width = mynumarray[0];
    if (width >= 100) {
      i = 0;
    } else {
      // width++;
      elem.style.width = width + "%";
      elem.innerHTML = width + "%";
    }
  });
});
