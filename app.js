var createError = require('http-errors');
var express = require('express');
var path = require('path');

const loadjsonfile = require('./appModules').loadjsonfile;

var logger = require('morgan');

var exphbs = require('express-handlebars');

//routes
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');

var app = express();

var sockIO = require('socket.io')();
app.sockIO = sockIO;


// view engine setup
// app.set('views', path.join(__dirname, 'views'));
// app.set('view engine', 'pug');
app.engine('handlebars', exphbs());
app.set('view engine', 'handlebars');;

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({
  extended: false
}));
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

// catch 404 and forward to error handler
app.use(function (req, res, next) {
  next(createError(404));
});

// error handler
app.use(function (err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  // res.status(err.status || 500);
  // res.render('error');
});


const ipc = require('node-ipc');


//running matlab in a subprocess--------------------
const fork = require('child_process').fork;
const program = path.resolve('background.js');
const parameters = [];
const options = {
  stdio: ['pipe', 'pipe', 'pipe', 'ipc']
};

const child = fork(program, parameters, options);




// IPC to communicate with routes

// ipc.config.id = 'appIPC';
// ipc.config.retry = 1500;
// ipc.config.silent = true;
// ipc.serve(() => ipc.server.on('rundynarematlab', message => {
//   console.log('received ipc message from router');
//   mymessage={'messageid': 'rundynarematlab'};
//   child.send(mymessage);
// })

// );
// ipc.server.start();

var initprogressbar = 0;
var initprogressbarBAYES = 0;
var initprogressbarSMOOTH = 0;

var progressbarcount = 0;
var progressbarcountBAYES = 0;
var progressbarcountSMOOTH = 0;


global.mysocket;
ipc.config.id = 'appIPC';
ipc.config.retry = 1500;
ipc.config.silent = true;
ipc.serve(() => ipc.server.on('rundynarematlab', (data, socket) => {
    global.mysocket = socket;
    console.log('received ipc message from router');
    console.log(data)

    if (data == 'estimation') {
      initprogressbar = 1;
      progressbarcount = 0;

      initprogressbarBAYES = 1;
      progressbarcountBAYES = 0;

      initprogressbarSMOOTH = 1;
      progressbarcountSMOOTH = 0;
    }

    mymessage = {
      'messageid': 'rundynarematlab',
      'messagetype': data
    };
    child.send(mymessage);
  })

);
ipc.server.start();


child.on('message', message => {

  var messageprocess = 0;

  if (message.messageid == 'matlabready') {
    console.log("matlabready");
    console.log(message.message);
    messageprocess = 1;

    if (message.message == 1) {
      console.log('-=stochastic simulation completed=-');

      finishstochastic();


      // ipc.server.emit(
      //   global.mysocket,
      //   'app.message',
      //   {
      //     id: ipc.config.id,
      //     message: 'Server emitted message'
      //   }
      // );

    }

    if (message.message == 2) {
      console.log('-=perfect foresight computation completed=-');
      finishperfect();
    }

    if (message.message == 3) {
      console.log('-=estimation computation completed=-');
      finishestimation();
    }

  }

  if (message.messageid == 'stdoutmessage') {
    // console.log("stdoutmessage");
    // console.log(`${message.message}`);
    // console.log(message.message);
    // console.log(message.messagetype);

    if (message.messagetype == 'progressbar') {
      if (initprogressbar == 1) {
        initprogressbar = 0;
        sockIO.emit('matlabmessagetoconsole_progressbar_init', message.message);
        console.log('matlabmessagetoconsole_progressbar_init sent');
      } else {

        progressbarcount = progressbarcount + 1;

        if (progressbarcount % 50 == 0) {
          sockIO.emit('matlabmessagetoconsole_progressbar', message.message);
          // console.log('matlabmessagetoconsole_progressbar');
        }


      }
    } else if (message.messagetype == 'progressbarBAYES') {
      if (initprogressbarBAYES == 1) {
        initprogressbarBAYES = 0;
        sockIO.emit('matlabmessagetoconsole_progressbarbayes_init', message.message);
      } else {
        progressbarcountBAYES = progressbarcountBAYES + 1;
        if (progressbarcountBAYES % 5 == 0) {
          sockIO.emit('matlabmessagetoconsole_progressbarbayes', message.message);
        }
      }
    } else if (message.messagetype == 'progressbarSMOOTH') {
      if (initprogressbarSMOOTH == 1) {
        initprogressbarSMOOTH = 0;
        sockIO.emit('matlabmessagetoconsole_progressbarsmooth_init', message.message);
      } else {
        progressbarcountSMOOTH = progressbarcountSMOOTH + 1;
        if (progressbarcountSMOOTH % 5 == 0) {
          sockIO.emit('matlabmessagetoconsole_progressbarsmooth', message.message);
        }
      }
    } else {
      console.log(message.message);
      sockIO.emit('matlabmessagetoconsole', message.message);
    }
    messageprocess = 1;
  }

  if (messageprocess == 0) {
    console.log('Message from child:', message.message, 'with id:', message.messageid);
  }
});
//---------------



//Initializing the DB and getting a model object
const initdb = require('./appModules').initdb;
initdb();


// sockIO.on('connection', function(socket){
//   console.log('A client connection occurred!');

// });

async function finishestimation() {

  // loading JSON output from Matlab
  try {
    estmatlabdata = await loadjsonfile('estimout.JSON');
  } catch (error) {
    return console.log('Unable to open estimout.JSON to process:' + error);
  }

  sockIO.emit('estimationfinish', estmatlabdata);

  console.log(estmatlabdata);

  return false;

}

async function finishstochastic() {

  // loading JSON output from Matlab
  try {
    stomatlabdata = await loadjsonfile('stochsimout.JSON');
  } catch (error) {
    return console.log('Unable to open stochsimout.JSON to process:' + error);
  }

  sockIO.emit('stochasticsimfinish', stomatlabdata);

  console.log(stomatlabdata);

  return false;

}

async function finishperfect() {

  // loading JSON output from Matlab
  try {
    permatlabdata = await loadjsonfile('perforout.JSON');
  } catch (error) {
    return console.log('Unable to open perforout.JSON to process:' + error);
  }

  sockIO.emit('perfectsimfinish', permatlabdata);

  console.log(permatlabdata);

  return false;

}




module.exports = app;
