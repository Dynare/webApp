//Custom modules for dynareApp callable from everywhere

const path = require('path');
const sqlite3 = require('sqlite3').verbose();
const spawn = require('child_process').spawn;
var fs = require('fs');
var short = require('short-uuid');




module.exports = {

  cl: function (content) {
    //writes to the console
    return (console.log(content));
  },

  showconsolealert: function (content, showflag) {
    //writes to the console
    let newcontent = $("textarea#preprocessorout").val() + content;
    $("textarea#preprocessorout").val(newcontent);
    if (showflag == 1) {
      $('.nav-tabs a[href="#console"]').tab('show');
    }

  },

  popthis: function (title, content) {
    //pops an alert
    $.alert({
      title: title,
      content: content,
    });
  },

  IsNumeric: function (input) {
    //is this a number ?
    input = input.replace(/\./g, '');
    return (input - 0) == input && ('' + input).trim().length > 0;

  },

  initdb: function () {
    //inits the DB and returns a model object

    var dbpath = path.resolve(__dirname, './assets/data/dynare.db');
    var mymodel;

    console.log(dbpath)

    let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE | sqlite3.OPEN_CREATE, (err) => {
      if (err) {
        console.log('problem here')
        console.error(err.message);
        return console.log(err.message);
      } else {
        console.log('Connected to the dynare database.');
      }
    });



    let sql = `SELECT COUNT(*) as mycount FROM sqlite_master WHERE type='table' AND name='dynaremodel';`;

    db.get(sql, [], (err, row) => {
      if (err) {
        return console.error(err.message);
      }
      if (row.mycount == 0) {
        //no tables in database, we create a new database
        modelhashuuid = short.generate();
        timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
        db.serialize(() => {
          // Queries scheduled here will be serialized.
          db.run(`CREATE TABLE dynaremodel ( id INTEGER NOT NULL, modelhash VARCHAR(100) NOT NULL, modelname VARCHAR(100) NOT NULL, modelcode TEXT, modelextrassinfo TEXT, modelextrasstype INTEGER, modelcreatedon DATETIME NOT NULL, modelupdatedon DATETIME NOT NULL, PRIMARY KEY (id), UNIQUE (modelhash) );`, [], function (err) {
            if (err) {
              return console.log(err.message);
            }
            // get the last insert id
            console.log(`dynaremodel created`);
          });
          db.run(`INSERT INTO dynaremodel(modelhash,modelname,modelcode,modelcreatedon,modelupdatedon) VALUES(?,?,?,?,?)`, [modelhashuuid, modelhashuuid, '%Enter model here', timestamp, timestamp], function (err) {
            if (err) {
              return console.log(err.message);
            }
            // get the last insert id
            console.log(`A row has been inserted with rowid ${this.modelhash}`);
          });
          // db.get('SELECT * FROM dynaremodel ORDER BY modelupdatedon DESC', [], (err, mymodel) => {
          //   if (err) {
          //     return console.error(err.message);
          //   } else {
          //     // console.log(mymodel);
          //     // createNewModel(mymodel);
          //   }
          // });

          db.close((err) => {
            if (err) {
              return console.error(err.message);
            }
          });
        });

      }
      // else {
      //   //there is a table in the database, we get the latest model
      //   db.serialize(() => {
      //     // We have to serialize in order to avoir race conditions
      //     db.get('SELECT * FROM dynaremodel ORDER BY modelupdatedon DESC', [], (err, mymodel) => {
      //       if (err) {
      //         return console.error(err.message);
      //       } else {
      //         // console.log(mymodel);
      //         // createNewModel(mymodel);
      //       }
      //     });
      //     db.close((err) => {
      //       if (err) {
      //         return console.error(err.message);
      //       }
      //     });
      //   });

      // }


    });



    return;
  },


  getmodelfromdb: function () {
    //gets the latest model from the db


    return new Promise(async function (resolve, reject) {


      let dbpath = path.resolve(__dirname, './assets/data/dynare.db');
      let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READONLY, (err) => {
        if (err) {
          return console.error(err.message);
        }
      });

      db.getAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.get(sql, sqloptions, function (err, data) {
            if (err)
              reject(err);
            else
              resolve(data);
          });
        });
      };

      db.closeAsync = function () {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.close(function (err) {
            if (err)
              reject(err);
            else
              resolve('db closed');
          });
        });
      };




      // getting updated dynare model in database async
      let sql2 = `SELECT * FROM dynaremodel ORDER BY modelupdatedon DESC;`;
      try {
        var dynaremodel = await db.getAsync(sql2);
      } catch (error) {
        return reject(new Error('Unable to get updated dynare model from database in getmodelfromdb: ' + error));
      }


      // closing database async
      try {
        await db.closeAsync();
      } catch (error) {
        return reject(new Error('Unable to close database in getmodelfromdb: ' + error));
      }

      // returning promise
      if (('modelhash' in dynaremodel)) {
        console.log('Model was found in database.');
        return resolve(dynaremodel)
      } else {
        return reject(new Error('modelhash was not found in dynaremodel in getmodelfromdb: ' + error));
      }



    });


  },





  savetodb: (savemodel) => {
    //check https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function

    return new Promise(async function (resolve, reject) {

      let dbpath = path.resolve(__dirname, './assets/data/dynare.db');
      let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
          return console.error(err.message);
        }
      });

      // db access promise functions definitions ---
      db.runAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.run(sql, sqloptions, function (err) {
            if (err)
              reject(err);
            else
              resolve('db updated');
          });
        });
      };

      db.getAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.get(sql, sqloptions, function (err, data) {
            if (err)
              reject(err);
            else
              resolve(data);
          });
        });
      };

      db.closeAsync = function () {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.close(function (err) {
            if (err)
              reject(err);
            else
              resolve('db closed');
          });
        });
      };
      // ---

      console.log("got to savetodb");

      // updating database async
      let sql1 = `UPDATE dynaremodel SET modelname = ?, modelcode = ?, modelupdatedon = ? WHERE modelhash = ?;`;
      timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
      try {
        await db.runAsync(sql1, [savemodel.modelname, savemodel.modelcode, timestamp, savemodel.modelhash]);
      } catch (error) {
        return reject(new Error('Unable to update database in savetodb: ' + error));
      }

      // getting updated dynare model in database async
      let sql2 = `SELECT * FROM dynaremodel WHERE modelhash = ?;`;
      try {
        var dynaremodel = await db.getAsync(sql2, [savemodel.modelhash]);
      } catch (error) {
        return reject(new Error('Unable to get updated dynare model from database in savetodb: ' + error));
      }

      // closing database async
      try {
        await db.closeAsync();
      } catch (error) {
        return reject(new Error('Unable to close database in savetodb: ' + error));
      }

      // returning promise
      if (('modelhash' in dynaremodel)) {
        console.log('Model updated and retrieved successfully in database.');
        return resolve(dynaremodel)
      } else {
        return reject(new Error('modelhash was not found in dynaremodel in savetodb: ' + error));
      }

    });
  },

  updatedbSS: (modelhash, sstext, sstype) => {
    //check https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function

    return new Promise(async function (resolve, reject) {

      let dbpath = path.resolve(__dirname, './assets/data/dynare.db');
      let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
          return console.error(err.message);
        }
      });

      // db access promise functions definitions ---
      db.runAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.run(sql, sqloptions, function (err) {
            if (err)
              reject(err);
            else
              resolve('db updated');
          });
        });
      };

      db.closeAsync = function () {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.close(function (err) {
            if (err)
              reject(err);
            else
              resolve('db closed');
          });
        });
      };
      // ---

      // updating database async
      let sql1 = `UPDATE dynaremodel SET modelextrassinfo = ?, modelextrasstype = ?, modelupdatedon = ? WHERE modelhash = ?;`;
      timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
      try {
        await db.runAsync(sql1, [sstext, sstype, timestamp, modelhash]);
      } catch (error) {
        return reject(new Error('Unable to update database in savetodb: ' + error));
      }

      // closing database async
      try {
        await db.closeAsync();
      } catch (error) {
        return reject(new Error('Unable to close database in savetodb: ' + error));
      }

      // returning promise
      return resolve()

    });
  },


  newmodelindb: (modelname) => {
    //check https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Statements/async_function

    return new Promise(async function (resolve, reject) {

      let dbpath = path.resolve(__dirname, './assets/data/dynare.db');
      let db = new sqlite3.Database(dbpath, sqlite3.OPEN_READWRITE, (err) => {
        if (err) {
          return console.error(err.message);
        }
      });

      // db access promise functions definitions ---
      db.runAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.run(sql, sqloptions, function (err) {
            if (err)
              reject(err);
            else
              resolve('db updated');
          });
        });
      };

      db.getAsync = function (sql, sqloptions) {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.get(sql, sqloptions, function (err, data) {
            if (err)
              reject(err);
            else
              resolve(data);
          });
        });
      };

      db.closeAsync = function () {
        var that = this;
        return new Promise(function (resolve, reject) {
          that.close(function (err) {
            if (err)
              reject(err);
            else
              resolve('db closed');
          });
        });
      };
      // ---

      modelhash = short.generate();

      // updating database async
      let sql1 = `INSERT INTO dynaremodel(modelhash,modelname,modelcode,modelcreatedon,modelupdatedon) VALUES(?,?,?,?,?);`;
      timestamp = new Date().toISOString().slice(0, 19).replace('T', ' ');
      try {
        await db.runAsync(sql1, [modelhash, modelname, '%Enter model here', timestamp, timestamp]);
      } catch (error) {
        return reject(new Error('Unable to insert new model in newmodelindb: ' + error));
      }

      // getting updated dynare model in database async
      let sql2 = `SELECT * FROM dynaremodel WHERE modelhash = ?;`;
      try {
        var dynaremodel = await db.getAsync(sql2, [modelhash]);
      } catch (error) {
        return reject(new Error('Unable to get new dynare model from database in newmodelindb: ' + error));
      }

      // closing database async
      try {
        await db.closeAsync();
      } catch (error) {
        return reject(new Error('Unable to close database in newmodelindb: ' + error));
      }

      // returning promise
      if (('modelhash' in dynaremodel)) {
        console.log('New model created and retrieved successfully in database.');
        return resolve(dynaremodel)
      } else {
        return reject(new Error('modelhash was not found in dynaremodel in newmodelindb: ' + error));
      }

    });
  },


  basedir: function (myfilename) {
    //retrives path to the console
    return (path.resolve(__dirname, './assets/modfiles/' + myfilename));
  },

  createnewfile: function (myfilename, filecontent) {
    return new Promise(function (resolve, reject) {
      var filePath = path.resolve(__dirname, './assets/modfiles/' + myfilename);
      fs.writeFile(filePath, filecontent, {
        flag: 'w'
      }, function (err) {
        if (err) return reject(new Error('Unable to createfile in createnewfile: ' + err));
        return resolve();
      });
    });
  },

  createnewfileatpath: function (myfilename, pathvalue, filecontent) {
    return new Promise(function (resolve, reject) {
      var filePath = path.resolve(__dirname, pathvalue + myfilename);
      fs.writeFile(filePath, filecontent, {
        flag: 'w'
      }, function (err) {
        if (err) return reject(new Error('Unable to createfile in createnewfile: ' + err));
        return resolve();
      });
    });
  },

  movenewdatafile: function (myfilename, myfiletype, myfilepath) {
    //moves an uploaded data file from its temporary directory to the work folder
    return new Promise(function (resolve, reject) {
      var filePath = path.resolve(__dirname, './assets/modfiles/' + 'datafile_' + myfilename + '.' + myfiletype);
      fs.rename(myfilepath, filePath, function (err) {
        if (err) return reject(new Error('Unable to move datafile to work folder: ' + err));
        return resolve();
      });
    });
  },

  loadjsonfile: function (myfilename) {
    return new Promise(function (resolve, reject) {

      console.log('inside loadjsonfile')

      var filePath = path.resolve(__dirname, './assets/modfiles/' + myfilename);
      fs.readFile(filePath, 'utf8', (err, fileContents) => {
        if (err) return reject(new Error('Unable to read file in readfile: ' + err));
        try {
          var jsondata = JSON.parse(fileContents)
        } catch (err) {
          return reject(new Error('Unable to load JSON in readfile: ' + err));
        }
        return resolve(jsondata);
      });
    });
  },

  loadjsonfileatpath: function (myfilename, pathvalue) {
    return new Promise(function (resolve, reject) {

      var filePath = path.resolve(__dirname, pathvalue + myfilename);
      fs.readFile(filePath, 'utf8', (err, fileContents) => {
        if (err) return reject(new Error('Unable to read file in readfile: ' + err));
        try {
          var jsondata = JSON.parse(fileContents)
        } catch (err) {
          return reject(new Error('Unable to load JSON in readfile: ' + err));
        }
        return resolve(jsondata);
      });
    });
  },

  removeoldfile: function (myfilename) {
    return new Promise(function (resolve, reject) {

      console.log(""); //leave this it helps with a weird bug

      var filePath = path.resolve(__dirname, './assets/modfiles/' + myfilename);
      fs.stat(filePath, function (err, stats) {
        if (!(err)) {
          fs.unlink(filePath, function (err) {
            if (err) return reject(new Error('Unable to delete existing file in removeoldfile: ' + err));
            return resolve();
          });
        }
        return resolve();
      });
    });
  },

  checkfileexist: function (myfilename) {
    return new Promise(function (resolve, reject) {
      var filePath = path.resolve(__dirname, './assets/modfiles/' + myfilename);
      fs.stat(filePath, function (err, stats) {
        if ((err)) {
          return resolve(false);
        } else {
          return resolve(true);
        }
      });
    });
  },

  checkfileexistatpath: function (myfilename, pathvalue) {
    return new Promise(function (resolve, reject) {
      var filePath = path.resolve(__dirname, pathvalue + myfilename);
      fs.stat(filePath, function (err, stats) {
        if ((err)) {
          return resolve(false);
        } else {
          return resolve(true);
        }
      });
    });
  },

  preprocess: function (myfilename, pptype) {
    return new Promise(function (resolve, reject) {

      var exePath = path.resolve(__dirname, './assets/preprocessor/dynare_m');
      var filePath = path.resolve(__dirname, './assets/modfiles/' + myfilename);
      var workPath = path.resolve(__dirname, './assets/modfiles/');

      var child;
      if (pptype == 1) {
        child = spawn(exePath, [filePath, 'json=parse', 'onlyjson']);
      }
      if (pptype == 2) {
        // cwd is important here
        child = spawn(exePath, [myfilename, 'console'], {
          cwd: workPath
        });
      }
      if (pptype == 3) {
        child = spawn(exePath, [filePath, 'json=parse']);
      }

      var dynarestdout = '';
      var dynareerror = '';

      child.stdout.on('data', (data) => {
        dynarestdout += data.toString();
        console.log(`child stdout:\n${data}`);
      });

      child.stderr.on('data', (data) => {
        console.error(`child stderr:\n${data}`);
        dynareerror += data.toString();
        // return reject(new Error(data));
      });

      child.on('exit', function (code, signal) {
        console.log('child process exited with ' + `code ${code} and signal ${signal}`);
        if (code == 0) {
          return resolve(dynarestdout);
        } else {
          //this will return the error output of the preprocessor and replace the local modfile path with just 'modfile'. The RegExp is used to replace all occurences
          return reject(new Error(dynarestdout.replace(new RegExp(filePath, 'g'), 'modfile') + 'Dynare preprocessor did not exit with code 0 in preprocess.\n\n'));
        }
      });
    });
  },

  preparejsonfiles: function (modelhash, runtype, jsonlist, jsonindata) {
    // writing a JSON file with the perfor table content to be sent to Matlab/Julia
    return new Promise(async function (resolve, reject) {

      console.log('inside preparejsonfiles')
      console.log(modelhash + '_ini.json')

      // loading model JSON file
      try {
        var modeljson = await module.exports.loadjsonfile(modelhash + '_ini.json');
      } catch (error) {
        return reject({
          'status': 100,
          'message': 'Unable to find necessary JSON file in preparejsonfiles\n' + error
        });
      }

      // console.log(modeljson)

      // removing old files
      try {
        await module.exports.removeoldfile('temp.m');
      } catch (error) {
        return reject({
          'status': 100,
          'message': error.toString()
        });
      }

      try {
        await module.exports.removeoldfile('temp_static.m');
      } catch (error) {
        return reject({
          'status': 100,
          'message': error.toString()
        });
      }

      try {
        await module.exports.removeoldfile('temp_dynamic.m');
      } catch (error) {
        return reject({
          'status': 100,
          'message': error.toString()
        });
      }

      try {
        await module.exports.removeoldfile('temp.mod');
      } catch (error) {
        return reject({
          'status': 100,
          'message': error.toString()
        });
      }



      // preparing temp.mod file content

      var modfilecontent = '';

      if ('endogenous' in modeljson) {
        modfilecontent += 'var ';
        Object.keys(modeljson['endogenous']).forEach((key, index) => {
          modfilecontent += modeljson['endogenous'][key]['longName'] + ' ';
        });
        modfilecontent += ';\n\n';
      }

      if ('exogenous' in modeljson) {
        modfilecontent += 'varexo ';
        Object.keys(modeljson['exogenous']).forEach((key, index) => {
          modfilecontent += modeljson['exogenous'][key]['longName'] + ' ';
        });
        modfilecontent += ';\n\n';
      }

      if ('parameters' in modeljson) {
        modfilecontent += 'parameters ';
        Object.keys(modeljson['parameters']).forEach((key, index) => {
          modfilecontent += modeljson['parameters'][key]['longName'] + ' ';
        });
        modfilecontent += ';\n\n';
      }

      if ('statements' in modeljson) {
        Object.keys(modeljson['statements']).forEach((key, index) => {
          if (modeljson['statements'][key]['statementName'] == 'param_init') {
            modfilecontent += modeljson['statements'][key]['name'] + ' = ' + modeljson['statements'][key]['value'] + ';\n';
          }
        });
        modfilecontent += '\n';
      }

      if ('model' in modeljson) {
        modfilecontent += 'model;\n';
        Object.keys(modeljson['model']).forEach((key, index) => {
          modfilecontent += modeljson['model'][key]['lhs'] + ' = ' + modeljson['model'][key]['rhs'] + ';\n';
        });
        modfilecontent += 'end;\n\n';
      }

      // managing the steady state status
      ssstatus = jsonlist["ssstatus"];
      ssdescription = jsonlist["ssdescription"];

      if (ssstatus == 0) {
        // this is the case where the user provided a steady_state block
        try {
          var ssjson = await module.exports.loadjsonfile(modelhash + '_ini_steady_state_model.json');
        } catch (error) {
          return reject({
            'status': 100,
            'message': 'No JSON file with steady-state found in preparejsonfiles\n' + error
          });
        }

        if ('steady_state_model' in ssjson) {
          modfilecontent += 'steady_state_model;\n';
          Object.keys(ssjson['steady_state_model']).forEach((key, index) => {
            modfilecontent += ssjson['steady_state_model'][key]['lhs'] + ' = ' + ssjson['steady_state_model'][key]['rhs'] + ';\n';
          });
          modfilecontent += 'end;\n';
        }
      }

      if (ssstatus == 55) {
        // this is the case when the user provided an initval block to compute the steady state
        if ('statements' in modeljson) {
          Object.keys(modeljson['statements']).forEach((key, index) => {
            if (modeljson['statements'][key]['statementName'] == 'init_val') {
              modfilecontent += 'initval;\n';
              var initvals = modeljson['statements'][key]['vals'];
              Object.keys(initvals).forEach((keyi, indexi) => {
                modfilecontent += initvals[keyi]['name'] + ' = ' + initvals[keyi]['value'] + ';\n';
              });
              modfilecontent += 'end;\n';
            }
          });
          modfilecontent += '\n';
        }
      }

      if (ssstatus == 99) {
        // this is the case when the user did not provide anything and the interface asked for more SS instructions
        ssdescription = jsonlist["ssdescription"][0];
        sstype = ssdescription['sstype'];

        if (sstype == 1) {
          // the user provided extra info as a steady-state block
          modfilecontent += 'steady_state_model;\n';
          modfilecontent += ssdescription['sstext'] + '\n';
          modfilecontent += 'end;\n';
          // saving extra ss info to DB
          try {
            await module.exports.updatedbSS(modelhash, ssdescription['sstext'], 1);
          } catch (error) {
            return reject({
              'status': 100,
              'message': 'Unable to update database in preparejsonfiles\n' + error
            });
          }
        } else if (sstype == 2) {
          // the user provided extra info as an initval block
          modfilecontent += 'initval;\n';
          modfilecontent += ssdescription['sstext'] + '\n';
          modfilecontent += 'end;\n';
          // saving extra ss info to DB
          try {
            await module.exports.updatedbSS(modelhash, ssdescription['sstext'], 2);
          } catch (error) {
            return reject({
              'status': 100,
              'message': 'Unable to update database in preparejsonfiles\n' + error
            });
          }
        } else {
          return reject({
            'status': 100,
            'message': 'No method for computing steady-state found in preparejsonfiles\n' + error
          });
        }
      }


      console.log('in preparejsonfiles before estimation writing');



      if (runtype == 3) {
        //for estimation we don't write a json file but write everything on the mod file

        try {
          await module.exports.removeoldfile('estimout.JSON');
        } catch (error) {
          return reject({
            'status': 100,
            'message': error.toString()
          });
        }

        //writing observables
        if ('obsdescription' in jsonindata) {
          modfilecontent += 'varobs ';
          Object.keys(jsonindata['obsdescription']).forEach((key, index) => {
            modfilecontent += modeljson['endogenous'][parseInt(jsonindata['obsdescription'][key]['obsindex'], 10)]['longName'] + ' ';
          });
          modfilecontent += ';\n\n';
        }

        //writing estimated params

        var priorname = ['dummy', 'dummy', 'beta_pdf', 'gamma_pdf', 'normal_pdf', 'uniform_pdf', 'inv_gamma_pdf', 'inv_gamma1_pdf', 'inv_gamma2_pdf', 'weibull_pdf'];

        if ('pardescription' in jsonindata) {

          // console.log(jsonindata['pardescription'])

          modfilecontent += 'estimated_params;\n';
          Object.keys(jsonindata['pardescription']).forEach((key, index) => {
            

            if (jsonindata['pardescription'][key]['parshockindex'] > 999) {
              if (jsonindata['pardescription'][key]['parassoshockindex'] > 0) {
                modfilecontent += "corr " + modeljson['exogenous'][parseInt(jsonindata['pardescription'][key]['parshockindex'], 10) - 1000]['longName'] + ',' + modeljson['exogenous'][parseInt(jsonindata['pardescription'][key]['parassoshockindex'], 10) - 1000]['longName'] + ', ';
              } else {
                modfilecontent += "stderr " + modeljson['exogenous'][parseInt(jsonindata['pardescription'][key]['parshockindex'], 10)-1000]['longName'] + ', ';
              }
            } else {
              modfilecontent += modeljson['parameters'][parseInt(jsonindata['pardescription'][key]['parshockindex'], 10)]['longName'] + ', ';
            }

            if (!(jsonindata['pardescription'][key]['parinivalue'] === null)) {
              modfilecontent += jsonindata['pardescription'][key]['parinivalue'] + ', ';
            } else {
              modfilecontent += 'NaN,';
            }

            if (!(jsonindata['pardescription'][key]['parLbound'] === null)) {
              modfilecontent += jsonindata['pardescription'][key]['parLbound'] + ', ';
            } else {
              modfilecontent += 'NaN,';
            }

            if (!(jsonindata['pardescription'][key]['parUbound'] === null)) {
              modfilecontent += jsonindata['pardescription'][key]['parUbound'] + ', ';
            } else {
              modfilecontent += 'NaN';
            }

            if (jsonindata['pardescription'][key]['parprior'] > 1) {
              modfilecontent +=','+ priorname[parseInt(jsonindata['pardescription'][key]['parprior'], 10)] + ', ';


              if (!(jsonindata['pardescription'][key]['parpriormean'] === null)) {
                modfilecontent += jsonindata['pardescription'][key]['parpriormean'] + ', ';
              }

              if (!(jsonindata['pardescription'][key]['parpriorstderr'] === null)) {
                modfilecontent += jsonindata['pardescription'][key]['parpriorstderr'] + ', ';
              }

              if (!(jsonindata['pardescription'][key]['parprior3par'] === null)) {
                modfilecontent += jsonindata['pardescription'][key]['parprior3par'] + ', ';
              } else {
                modfilecontent += 'NaN,';
              }

              if (!(jsonindata['pardescription'][key]['parprior4par'] === null)) {
                modfilecontent += jsonindata['pardescription'][key]['parprior4par'] + ', ';
              } else {
                modfilecontent += 'NaN,';
              }


              if (!(jsonindata['pardescription'][key]['parpriorscale'] === null)) {
                modfilecontent += jsonindata['pardescription'][key]['parpriorscale'] + ', ';
              }
              else{
                modfilecontent += 'NaN';
              }

            }

            modfilecontent += ';\n';

            // modfilecontent += modeljson['endogenous'][parseInt(jsonindata['obsdescription'][key]['obsindex'], 10)]['longName']+ ' ';
          });
          modfilecontent += 'end;\n\n';
        }

        //writing estimation command

        // console.log(jsonindata['nobs'])
        // console.log(jsonindata['mhreplic'])
        // console.log(jsonindata['mhnblocks'])
        // console.log(jsonindata['mhjscale'])

        

        modfilecontent +="estimation(datafile='datafile_"+modelhash+"."+jsonindata['datafiletype']+"'"; 

        
        // if (!(jsonindata['nobs'] === null)) {
        //   modfilecontent +=',nobs='+parseInt(jsonindata['nobs'], 10);
        // }

        // if (!(jsonindata['mhreplic'] === null)) {
        //   modfilecontent +=',mh_replic='+parseInt(jsonindata['mhreplic'], 10);
        // }

        // if (!(jsonindata['mhnblocks'] === null)) {
        //   modfilecontent +=',mh_nblocks='+parseInt(jsonindata['mhnblocks'], 10);
        // }

        // if (!(jsonindata['mhjscale'] === null)) {
        //   modfilecontent +=',mh_jscale='+parseInt(jsonindata['mhjscale'], 10);
        // }

        // //put all the estimation options here
        console.log('---===== ESTIM OPTIONS ====---')
        var option0flag=null;
        Object.keys(jsonindata['optionsdescription']).forEach((key, index) => {

          if (jsonindata['optionsdescription'][key]['estoptinputtype'] == 0){//no input options
            modfilecontent +=","+jsonindata['optionsdescription'][key]['estoptdynarename'];
          }
          else {//options with input
            if (jsonindata['optionsdescription'][key]['estoptindex']==0){
              option0flag=jsonindata['optionsdescription'][key]['estoptvalue'];
            }
            else {
            modfilecontent +=","+jsonindata['optionsdescription'][key]['estoptdynarename']+"="+jsonindata['optionsdescription'][key]['estoptvalue'];
            }
          }

        });


        //end of options close estimation command
        if (option0flag !=null){//there is a specific list of endogenous to be added behind the estimation command
          modfilecontent +=') '+option0flag+';'
        }
        else{//we add no endogenous behind the estimation command
          modfilecontent +=');'
        }
        









      }

      console.log('in preparejsonfiles AFTER estimation writing');

      // writing the temp.mod file
      try {
        await module.exports.createnewfile('temp.mod', modfilecontent);
      } catch (error) {
        return reject({
          'status': 100,
          'message': 'Unable to create temp.mod in preparejsonfile\n' + error
        });
      }

      // normal preprocessing that mod file
      var dynarestdout = '';
      try {
        dynarestdout += await module.exports.preprocess('temp.mod', 2);
      } catch (error) {
        return reject({
          'status': 100,
          'message': 'Unable to preprocess temp.mod in preparejsonfile\n' + error
        });
      }

      // removing old JSON files and writing the correct file
      if (runtype == 1) {
        //writes JSON file for perfect foresight
        try {
          await module.exports.removeoldfile('perforout.JSON');
        } catch (error) {
          return reject({
            'status': 100,
            'message': error.toString()
          });
        }
        try {
          await module.exports.removeoldfile('perforin.JSON');
        } catch (error) {
          return reject({
            'status': 100,
            'message': error.toString()
          });
        }
        try {
          await module.exports.createnewfile('perforin.JSON', JSON.stringify(jsonindata));
        } catch (error) {
          return reject({
            'status': 100,
            'message': 'Unable to create perforin.JSON in preparejsonfile\n' + error
          });
        }
      }

      if (runtype == 2) {
        //writes JSON file for stochastic simulations
        try {
          await module.exports.removeoldfile('stochsimout.JSON');
        } catch (error) {
          return reject({
            'status': 100,
            'message': error.toString()
          });
        }
        try {
          await module.exports.removeoldfile('stochsimin.JSON');
        } catch (error) {
          return reject({
            'status': 100,
            'message': error.toString()
          });
        }
        try {
          await module.exports.createnewfile('stochsimin.JSON', JSON.stringify(jsonindata));
        } catch (error) {
          return reject({
            'status': 100,
            'message': 'Unable to create stochsimin.JSON in preparejsonfile\n' + error
          });
        }
      }

      // if (runtype == 3) {
      //   //writes JSON file for estimation
      //   try {
      //     await module.exports.removeoldfile('estimout.JSON');
      //   } catch (error) {
      //     return reject({
      //       'status': 100,
      //       'message': error.toString()
      //     });
      //   }
      //   try {
      //     await module.exports.removeoldfile('estimin.JSON');
      //   } catch (error) {
      //     return reject({
      //       'status': 100,
      //       'message': error.toString()
      //     });
      //   }
      //   try {
      //     await module.exports.createnewfile('estimin.JSON', JSON.stringify(jsonindata));
      //   } catch (error) {
      //     return reject({
      //       'status': 100,
      //       'message': 'Unable to create estimin.JSON in preparejsonfile\n' + error
      //     });
      //   }
      // }



      return resolve();
    }); //end of promise function
  }

} //module end
